#!/bin/sh

echo "###############################"
echo " BOOTING SAMPLE REACT FRONTEND"
echo "     WWW.YOUR-DOMAIN.LOCAL"
echo "###############################"


# FIRST_BOOT_GUARD=$(dirname $0)/firstbootguard
# if [[ ! -f $FIRST_BOOT_GUARD ]]; then
#   echo "########################"
#   echo "   RUNNING FIRST-BOOT"
#   echo "########################"

#   touch $FIRST_BOOT_GUARD

#   # you could put first-boot-only things in here
#   # for example, creating the dev database, seeding dev data, etc.
#   # this is just an idea, take it or leave it
# fi

yarn start
